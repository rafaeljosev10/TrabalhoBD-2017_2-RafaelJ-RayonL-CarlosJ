<?php
ob_start();
session_start();
include ("connect.php");
if(isset($_POST['addcli'])){
  $nome_cliente = trim(strip_tags($_POST["nome_cliente"]));
  $rg = trim(strip_tags($_POST["rg"]));
  $cpf = trim(strip_tags($_POST["cpf"]));
  $data_nasc_cliente = trim(strip_tags($_POST["data_nasc_cliente"]));
  $tipo_conta = trim(strip_tags($_POST["tipo_conta"]));
  $tipo_endereco = trim(strip_tags($_POST["tipo_endereco"]));
  $endereco = trim(strip_tags($_POST["endereco"]));
  $numero_casa = trim(strip_tags($_POST["numero_casa"]));
  $bairro = trim(strip_tags($_POST["bairro"]));
  $complemento = trim(strip_tags($_POST["complemento"]));
  $cep = trim(strip_tags($_POST["cep"]));
  $cidade = 'fdp';//trim(strip_tags($_POST["cidade"]));
  $estado = 'pnc';//trim(strip_tags($_POST["estado"]));
  $senha = trim(strip_tags($_POST["senha"]));

  //$cidades_id = "select * from Cidades where nome_cidade =:cidade and estado =:estado";
 try{
    $output_cidades = $conexao->prepare("select * from Cidades where nome_cidade =:cidade and estado =:estado");
    $output_cidades->bindParam(':cidade', $cidade, PDO::PARAM_STR);
    $output_cidades->bindParam(':estado', $estado, PDO::PARAM_STR);
    $output_cidades->execute();
    $result_cid = $output_cidades->FETCH(PDO::FETCH_ASSOC);
    if(!isset($result_cid['id'])){
      $insert_cid = $conexao->prepare("insert into Cidades (nome_cidade, estado) values (:cidade, :estado)");
      $insert_cid->bindParam(':cidade', $cidade, PDO::PARAM_STR);
      $insert_cid->bindParam(':estado', $estado, PDO::PARAM_STR);
      $insert_cid->execute();
    }else{
      echo 'cidade e estado já existem. id selecionado ao inves de adicionado';
    }
  }catch(PDOException $e){
    echo $e;
  }
  try{
    $output_enderecos = $conexao->prepare("select id from Enderecos where cep =:cep");
    $output_enderecos->bindParam(':cep', $cep, PDO::PARAM_STR);
    $output_enderecos->execute();
    $resut_end = $output_enderecos->FETCH(PDO::FETCH_ASSOC);
    if(!isset($result_end['id'])){
      $insert_end = $conexao->prepare("insert into Enderecos (tipo, logradouro, bairro, complemento, cep, numero, cidades_id) values (:tipo_end, :logradouro, :bairro, :complemento, :cep, :numero_casa, :cidades_id)");
      $insert_end->bindParam(':tipo_end', $tipo_endereco, PDO::PARAM_STR);
      $insert_end->bindParam(':logradouro', $endereco, PDO::PARAM_STR);
      $insert_end->bindParam(':bairro', $bairro, PDO::PARAM_STR);
      $insert_end->bindParam(':complemento', $complemento, PDO::PARAM_STR);
      $insert_end->bindParam(':cep', $cep, PDO::PARAM_STR);
      $insert_end->bindParam(':numero_casa', $numero_casa, PDO::PARAM_STR);
      $insert_end->bindParam(':cidades_id', $result_cid['id'], PDO::PARAM_STR);
      $insert_end->execute();
    }else{
      echo 'endereço já existe. id selecionado ao inves de adicionado';
    }
  }catch(PDOException $e){
    echo $e;
  }
  try{
  $output_cliente = $conexao->prepare('select * from Cliente where cpf =:cpf');
    $output_cliente->bindParam(':cpf', $cpf, PDO::PARAM_INT);
    $output_cliente->execute();
    $result_cli = $output_cliente->FETCH(PDO::FETCH_ASSOC);
    echo $result_cli['cpf'];
    $result_end = 1;
  if(!isset($result_cli['cpf'])){
      $insert_cli = $conexao->prepare("insert into Cliente (cpf, rg, data_nasc_cliente, enderecos_id, nome, Senha) values (:cpf, :rg, :data_nasc_cliente, :enderecos_id, :nome_cliente, :senha)");
      $insert_cli->bindparam(':cpf', $cpf, PDO::PARAM_INT);
      $insert_cli->bindparam(':rg', $rg, PDO::PARAM_STR);
      $insert_cli->bindparam(':data_nasc_cliente', $data_nasc_cliente, PDO::PARAM_STR);
      $insert_cli->bindparam(':enderecos_id', $result_end, PDO::PARAM_INT);
      $insert_cli->bindparam(':nome_cliente', $nome_cliente, PDO::PARAM_STR);
      $insert_cli->bindparam(':senha', MD5($senha), PDO::PARAM_STR);
      $insert_end->execute();
    }else{
      echo 'Cliente já existe';
    }
  }catch(PDOException $e){
    echo $e;
  }

}
/*
$cliente_cpf = "select * from Cliente where cpf = {$cpf}";
$query_cliente = mysqli_query($conexao, $clientes_cpf);
$output_cliente = mysqli_fetch_assoc($query_cliente);
if (empty($output_cliente)){
  $fk_enderecos = $output_enderecos['id'];
  $result = mysqli_query($conexao, "INSERT INTO Cliente (cpf, rg, data_nasc_cliente, enderecos_id, nome, Senha) VALUES ({$cpf},'{$rg}',{$data_nasc_cliente}, {$fk_enderecos},'{$nome_cliente}', MD5('{$senha}'))");
}else{
  echo 'CPF já existe, cliente não adicionado';
}
if($result){?>
  <div class="alert-success">
    <p>Cliente <?php echo $nome_cliente; ?>Adicionado com sucesso</p>
  </div>
<?php }else{ ?>
  <div class="alert-danger">
    <h5>Cliente <?php echo $nome_cliente; ?> Não Adicionado </h5>
  </div>
<?php }*/?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Cadastro - NullBank</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="../vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>
  <body class="login">
    <div class="right_col" role="main">
      <div class="">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
              <div class="x_title">
                <h2>Cadastro de cliente <small>NullBank</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">

                <form class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">

                  <p>Faça parte da nova geração de serviços financeiros do Brasil.</a>
                  </p>
                  <span class="section">Informações Pessoais</span>

                  <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nome Completo<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                      <input type="text" class="form-control has-feedback-left" id="inputSuccess2" name="nome_cliente" placeholder="Ex.: João da Silva Santos">
                      <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">RG<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <input type="name" id="rg" name="rg" required="required" placeholder="" class="form-control col-md-7 col-xs-12">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">CPF<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <input type="number" id="cpf" name="cpf" required="required" placeholder="00000000000" class="form-control col-md-7 col-xs-12">
                    </div>
                  </div>

                  <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="datanasc">Data de Nascimento <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                          <input type='text' class="form-control col-md-3 col-sm-1 col-xs-9" name="data_nasc_cliente" placeholder="31-12-2000"/>
                      </div>
                  </div>

                  <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Endereço <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <select id="heard" class="form-control" name="tipo_endereco"required>
                        <option value="">Selecione..</option>
                        <option value="Aeroporto">Aeroporto</option>
                        <option value="Alameda">Alameda</option>
                        <option value="Área">Área</option>
                        <option value="Avenida">Avenida</option>
                        <option value="Campo">Campo</option>
                        <option value="Chácara">Chácara</option>
                        <option value="Colônia">Colônia</option>
                        <option value="Condomínio">Condomínio</option>
                        <option value="Conjunto">Conjunto</option>
                        <option value="Distrito">Distrito</option>
                        <option value="Esplanada">Esplanada</option>
                        <option value="Estação">Estação</option>
                        <option value="Favela">Favela</option>
                        <option value="Fazenda">Fazenda</option>
                        <option value="Feira">Feira</option>
                        <option value="Jardim">Jardim</option>
                        <option value="Ladeira">Ladeira</option>
                        <option value="Lago">Lago</option>
                        <option value="Lagoa">Lagoa</option>
                        <option value="Largo">Largo</option>
                        <option value="Loteamento">Loteamento</option>
                        <option value="Morro">Morro</option>
                        <option value="Núcleo">Núcleo</option>
                        <option value="Parque">Parque</option>
                        <option value="Passarela">Passarela</option>
                        <option value="Pátio">Pátio</option>
                        <option value="Praça">Praça</option>
                        <option value="Quadra">Quadra</option>
                        <option value="Recanto">Recanto</option>
                        <option value="Residencial">Residencial</option>
                        <option value="Rodovia">Rodovia</option>
                        <option value="Rua">Rua</option>
                        <option value="Setor">Setor</option>
                        <option value="Sítio">Sítio</option>
                        <option value="Travessa">Travessa</option>
                        <option value="Trecho">Trecho</option>
                        <option value="Trevo">Trevo</option>
                        <option value="Vale">Vale</option>
                        <option value="Vereda">Vereda</option>
                        <option value="Via">Via</option>
                        <option value="Viaduto">Viaduto</option>
                        <option value="Viela">Viela</option>
                        <option value="Vila">Vila</option>
                      </select>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Logradouro<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <input type="name" id="address" name="endereco" required="required" placeholder="Paulista" class="form-control col-md-7 col-xs-12">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Numero
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <input type="name" id="houseNumber" name="numero_casa" placeholder="100" class="form-control col-md-7 col-xs-12">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Bairro<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <input type="name" id="neighbor" name="bairro" required="required" placeholder="Bela Vista" class="form-control col-md-7 col-xs-12">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Complemento
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <input type="name" id="aditionalAddress" name="complemento" placeholder="Aptº 201" class="form-control col-md-7 col-xs-12">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">CEP <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="name" id="cep" name="cep" placeholder="13145-034" class="form-control col-md-7 col-xs-12">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Cidade<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <input type="name" id="city" name="cidade" required="required" placeholder="São Paulo" class="form-control col-md-7 col-xs-12">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Estado<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <input type="name" id="state" name="estado" required="required" placeholder="São Paulo" class="form-control col-md-7 col-xs-12">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Senha<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <input type="password" id="cliente_password" name="senha" required="required" placeholder="" class="form-control col-md-7 col-xs-12">
                    </div>
                  </div>
                  <div class="ln_solid"></div>
                  <div class="form-group">
                    <div class="col-md-6 col-md-offset-3">
                      <input type="submit" name="addcli" value="Adicionar" class="btn btn-success">
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<!-- /page content -->
<?php include("footer.php")?>
