<?php
ob_start();
session_start();
if((isset($_COOKIE['usuario']) && isset($_COOKIE['senha']))){
  header("Location: user.php");exit;
}
include("connect.php");
if(isset($_POST['loginbd'])){
  $login_bd = $_POST["username"];
  $pw_bd = $_POST["password"];
  setcookie("usuario", $login_bd, time()+7200); //expira em 2 horas
  setcookie("senha", $pw_bd, time()+7200);
  header('Location: user.php');
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Conexão com o Banco de Dados </title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="../vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>
  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form action="#" method="post" enctype="multipart/form-data">
              <br><h1>Conexão com o Banco de Dados</h1>
              <div>
                <input type="text" class="form-control" value="root" name ="username" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" name="password" required="" />
              </div>
              <div>
                <input type="submit" name="loginbd" value="Conectar" class="btn btn-success submit">
              </div>

              <div class="clearfix"></div>

              <div class="separator">

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-money"></i> Nullbank</h1>
                  <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                </div>
              </div>
            </form>
          </section>
        </div>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>
