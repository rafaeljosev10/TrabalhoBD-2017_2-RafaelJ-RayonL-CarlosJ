<?php
ob_start();
session_start();
if((isset($_SESSION['funcionarionullbank']) && isset($_SESSION['senhafunc']))){
  header("Location: home-funcionario.php");exit;
}
  include("connect.php");
  if(isset($_POST['loginusr'])){
    //Recupera os dados
    $matricula = trim(strip_tags($_POST['matricula']));
    $senha_func = trim(strip_tags($_POST['senhaf']));
    //Seleciona BD
    $select = "select * from Funcionario where matricula =:matricula and senha =:senha";

    try{
      $output_username = $conexao->prepare($select);
      $output_username->bindParam(':matricula', $matricula, PDO::PARAM_STR);
      $output_username->bindParam(':senha', md5($senha_func), PDO::PARAM_STR);
      $output_username->execute();
      $count = $output_username->rowCount();
      if($count>0){
        $matricula = $_POST['matricula'];
        $senha_func = $_POST['senhaf'];
        $_SESSION['funcionarionullbank'] = $matricula;
        $_SESSION['senhafunc'] = $senha_func;
        header("Refresh:1, login-success.php");
      }else{
        header("Refresh:1, login-failed.php");
      }
    }catch(PDOException $e){
      echo $e;
    }
  } // se o botão de login foi clicado

 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Funcionario - acesso </title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="../vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>
  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <h1>Bem vindo Funcionário</h1>
            <form action="#" method="post" enctype="multipart/form-data">
              <div>
                <h5>Matricula</h5>
                <input type="text" class="form-control" placeholder="Insira matrícula" name ="matricula" required="" />
              </div>
              <div>
                <h5>Senha</h5>
                <input type="password" class="form-control" placeholder="" name="senhaf" required="" />
              </div>
              <div>
                <input type="submit" name="loginusr" value="Login" class="btn btn-success submit">
              </div>

              <div class="clearfix"></div>

              <div class="separator">

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-money"></i> Nullbank</h1>
                  <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>
