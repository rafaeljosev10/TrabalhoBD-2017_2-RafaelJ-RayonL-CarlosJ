Consultas

SELECT  F.Conta,  F.Agencia, F.nomeAgencia,
(SELECT COUNT(*) FROM `1.5` AS D WHERE D.Conta = F.Conta AND Datas BETWEEN 'DATA1' AND 'DATA2') AS Quantidade
 FROM `1.5` AS F WHERE F.Datas BETWEEN 'DATA1' AND 'DATA2' GROUP BY F.Conta;
 
SELECT idContas AS Conta, SUM(valor) AS Volume, Agencia, nomeAgencia FROM `1.6` WHERE Data_hora BETWEEN '2017-11-28 22:54:10' AND now() Group by idContas ORDER BY Volume DESC;

SELECT DISTINCT Conta,cpf,nome,(
SELECT COUNT(*) FROM transacoes t WHERE Conta = Conta_id AND t.Data_hora BETWEEN 'DATA1' AND 'DATA2'
) AS Quantidade FROM `2.3` WHERE Data_hora BETWEEN 'DATA1' AND 'DATA2' ORDER BY Quantidade;
